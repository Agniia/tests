<?php
	include __DIR__.'/page_components/header.php';
    $header = $pageComponents->getHeader($canUploadTest, $canReadListOfTest);
	$content = '';
    if($_SERVER['REQUEST_METHOD'] == 'GET')
	 {
	 	 if($authorization->checkUserSession())
		 {
		 	 $content = '<h1>Авторизация</h1>';
		 	 $content .= '<h3>Вы авторизованы как '.$_SESSION['login'].'</h3>';	
		 }
		 else
		 {
		 	  $content =  '<h1>Авторизация</h1>';
		 	  $content .= $authorization->showForm(); 
		 }
	 }
	else if (isset($_POST["login"]) && isset($_POST["password"]))
    {
        header('Location: authorization.php');
        $login = strip_tags($_POST["login"]);
        $password  = strip_tags($_POST["password"]);
        $isAuth = $authorization->checkUser($login, $password);   
        if($isAuth){
         	  $content = '<h1>Авторизация</h1>';
		 	  $content .= '<h3>Вы авторизованы как '.$_SESSION['login'].'</h3>';	
         }
        else
        {
        	 $content = '<h1>Авторизация</h1>';
        	 $content .= $authorization->showAuthorizationError(); 
	 		 $content .= $authorization->showForm(); 
        }
    }
	echo  $header;
    echo $content;
	 
   include __DIR__.'/page_components/footer.php';
<?php
    header('Content-Type: text/html; charset=utf-8');
    function myAutoloader($className)
    {
       $rootDir = str_replace('page_components','',__DIR__);               
       $file = str_replace('\\',DIRECTORY_SEPARATOR,$className);
       $fileName = $rootDir .'classes'.DIRECTORY_SEPARATOR . $file . '.php'; 
       if(file_exists($fileName))
        {
    		include $fileName;
        }
    }
    spl_autoload_register('myAutoloader');
    
    $authorization = new \Authorization\Authorization();
    
    $sessionID = $authorization->startSession();
    
    $pageComponents = new \PageComponents\PageComponents(); 
    
    $canUploadTest = ($authorization->checkUserCanUploadTest()) ? true : false;
    $canReadListOfTest = ($authorization->checkUserCanReadListOfTest()) ? true : false;
    $dataInstance = new \Data\Data(); 
    if(isset($_POST["logout"]))
	{	       
	     $authorization->logOut();
	     header('Location: authorization.php');
    }
<?php
    include __DIR__.'/page_components/header.php';
    if(!$authorization->checkUserSession())
    {
 			$header = $pageComponents->getHeader($canUploadTest, $canReadListOfTest);
   		    echo  $header;
 			$authorization->showForbidden();
 			include 'page_components/footer.php';
 			die;
    }
    $header = $pageComponents->getHeaderForTest($canUploadTest, $canReadListOfTest);
    echo  $header;
	if(!empty($_GET)&&array_key_exists('test', $_GET) )
	{
        $filename = $dataInstance->getTestNamefromGet($_GET['test'] );
        if($filename == ''){
        	$dataInstance->testIsNotFound();
        } 
        echo   '<h1>Тест № '.$_GET['test'] .'</h1>';
        $questionArray =  $dataInstance->getTestContent($filename);
        if(empty($questionArray)) die('Ошибка декодирования json');
        echo '<h2 style="margin: 0px auto 30px; text-align:center">'. $questionArray['testName'].'</h2>';
        echo '<form action="" method="POST">';
        if(!empty($questionArray)){
            $count = 1;
            foreach($questionArray['questions'] as $question => $questionDetails){                  
                 echo '<fieldset style="margin-bottom: 20px;"><legend style="margin-bottom: 15px;">'.$questionDetails['questionName'].'</legend>';
                    foreach($questionDetails as $key => $questionDetail){
                        if($key == 'questionName') continue;
                        if($key == 'true' ) continue;
                        echo '<label  style="margin-right: 30px;"><input style="margin-right: 10px;" type="radio" name="q'.$count.'" value="'.$questionDetail. '">'. $questionDetail .'</label>';
                    }
                 echo '</fieldset>';
                 $count++;
            }
        }     
        echo  '<button type="submit" class="btn btn-primary">Отправить</button>';
        echo '</form>'; 
       if(!empty($_POST))
       {
	       $userName = $_SESSION['login'];
	       $good = 'Вы верно ответили на вопрос(ы): <br>';
	       $bad = 'Вы не верно ответили на вопрос(ы): <br>';
	       $countGood=0;  
	       $countBad=0;        
			$fileName = $dataInstance->getTestNamefromGet($_GET['test'] );
	        $questionArray =  $dataInstance->getTestContent($fileName);
	       if(empty($questionArray)) die('Ошибка декодирования json');
	       foreach($_POST as $key => $value){
		         if($questionArray["questions"][$key]['true'] == $value){
		                    $good .= $questionArray["questions"][$key]['questionName'] ;
		                    $good .= ' (Верный ответ  '.$questionArray["questions"][$key]['true'].'); <br>';
		                    $countGood++;
		         }else{
		                    $bad .= $questionArray["questions"][$key]['questionName'] . ' (Верный ответ  '.$questionArray["questions"][$key]['true'].'); <br>';
		                     $countBad++;
		         }
	        }
	        $_SESSION["text_1"] = 'Уважаемый, '. $userName.', '; 
	        $_SESSION["text_2"] = 'в тесте "'. $questionArray['testName'].'"'; 
	        $_SESSION["text_3"] = 'Вы правильно ответили на '. $countGood .' вопросов';    
	        $_SESSION["text_4"] = 'Вы не правильно ответили на '. $countBad .' вопросов'; 
	        echo '<img style="display: block; margin: 30px auto;" src="sertificate.php">';
        }
    }
    include __DIR__.'/page_components/footer.php';
 ?>
<?php
//header ("Content-type: image/png"); 
session_start();
$image = imagecreatetruecolor(600, 400);
$backColor =  imagecolorallocate ( $image , 255 , 204 , 75);
$textColor =  imagecolorallocate ( $image , 0 , 15 , 90);
imagefill ( $image , 0 ,0 , $backColor );

$font = __DIR__.'/resources/fonts/Arial_Cyr.ttf';
$top = 50;
foreach ($_SESSION as $key=>$value)
{
       $arr = explode('_',$key);
       if( $arr[0] == 'text'){
	        $top += 50;
	        imagettftext($image, 20, 0, 50,$top, $textColor, $font, $value);
	        unset($_SESSION[$key]);
	   }
} 

imagepng($image);
imagedestroy($image);
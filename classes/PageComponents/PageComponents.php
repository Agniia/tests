<?php
namespace PageComponents; 

class PageComponents
{		
    public function getMenu($canUploadTest, $canReadListOfTest)
	{
		$html = '<nav class="main-menu"><ul class="row justify-content-between align-items-center"><li><a href="index.php">Главная</a></li>';
	   if($canReadListOfTest)
           	$html .= '<li><a href="list.php">Спиcок тестов</a></li>';
       if($canUploadTest)
           	$html .= '<li><a href="admin.php">Добавить новый тест</a></li>';
       	$html .=  '<li><a href="authorization.php">Авторизация</a></li><li><form action="" method="post"><input type="submit" name="logout" value="Выйти" class="btn btn-secondary"></form></li></ul></nav>';	
		return $html;
	}
	 public function getHeaderForTest($canUploadTest, $canReadListOfTest)
	 {
			$html = '<!DOCTYPE html>
						<html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><title>Tests</title><link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
						<style>
						    html,body{
						        height: 100%;
						    }
						    div, ul, p{
						         padding: 0;
						         margin: 0;
						    }
				            h1, form, .panel{
				                width: 80%;
				                margin: 50px auto 40px;
				            }
				            h1, h2, form{
				                text-align: center;
				            }
				            h2{
				            	margin: 0 auto 40px;
				            }
				            header, footer{
				                color: #fff;
				                font-size: 0.8rem;
				                background: #000;
				                height: 100px;
				            }
				            .horizontal-padding{
				                padding-left: 25px;
				                padding-right: 25px;
				            }
				            .main-menu ul {
				                list-style-type: none;
				            } 
				           .main-menu a{
				                color: #fff;
				            }
				            .main-menu a:hover{
				                text-decoration: none;
				            }
				            .main-content{
				                flex: 2; 
				            }
				        </style>
	    </head>
	    <body>   
	    <div class="main-container d-flex flex-column">
	    <header class="d-flex align-items-center horizontal-padding">
	    <div class="container-fluid">';  
	    $html .= $this->getMenu($canUploadTest, $canReadListOfTest);
	    $html .= '</div></header><div class="main-content horizontal-padding container-fluid">';
	 	return $html;

	}
	 public function getHeader($canUploadTest, $canReadListOfTest)
	 {
	 	$html = '<!DOCTYPE html>
						<html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><title>Tests</title><link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
						<style>
						    html,body{
						        height: 100%;
						    }
						    div, ul, p{
						         padding: 0;
						         margin: 0;
						    }
				            h1, form, .panel{
				                width: 80%;
				                margin: 50px auto 40px;
				            }
				            h1, h2, form{
				                text-align: center;
				            }
				            h2{
				            	margin: 0 auto 40px;
				            }
				            header, footer{
				                color: #fff;
				                font-size: 0.8rem;
				                background: #000;
				                flex: 0.5;
				            }
				            .horizontal-padding{
				                padding-left: 25px;
				                padding-right: 25px;
				            }
				            .main-menu ul {
				                list-style-type: none;
				            } 
				           .main-menu a{
				                color: #fff;
				            }
				            .main-menu a:hover{
				                text-decoration: none;
				            }
				            .main-container{
				                height: 100%;
				            }
				            .main-content{
				                flex: 2; 
				            }
				        </style>
	    </head>
	    <body>   
	    <div class="main-container d-flex flex-column">
	    <header class="d-flex align-items-center horizontal-padding">
	    <div class="container-fluid">';  
	    $html .= $this->getMenu($canUploadTest, $canReadListOfTest);
	    $html .= '</div></header><div class="main-content horizontal-padding container-fluid">';
	 	return $html;
	 }
}
<?php
namespace Data; 

class Data
{	
	
	public function getListOfTests()
	{
		$rootDir = str_replace('classes/Data','',__DIR__);       
		return glob($rootDir . 'uploads/json/*.json');
	}
	
	public function saveNewTest($testName)
	{
	        $rootDir = str_replace('classes/Data','',__DIR__);       
	        $number = count(glob($rootDir . 'uploads/json/*.json'));
            $filename = $_FILES['testFile']['tmp_name'];
            $dist = $rootDir . 'uploads/json/'.$testName.'_test_'.$number.'_.json';
            move_uploaded_file($filename, $dist);
	}
	
    public function getTestNamefromGet($getParam)
	{
		 $rootDir = str_replace('classes/Data','',__DIR__);           
		 $arr = glob($rootDir. 'uploads/json/*'.$getParam.'_.json');
	     if(count($arr) == 0) return '';
	     return $arr[0];
	}
    
    public function getTestContent($filename)
	{
	    if(!file_exists ($filename)) return [];
	    $jsonData = file_get_contents($filename);  
	    $questionArray = json_decode($jsonData, true); 
	    if(json_last_error() !== JSON_ERROR_NONE) $questionArray = [];
	    return $questionArray;
	}
    
    public function testIsNotFound(){
		  	http_response_code(404);
            die('404 Not Found');
	}
}
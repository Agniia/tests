<?php
namespace Authorization; 

class Authorization
{
	protected $currentSessionID;
	protected	$usersData;
	
	public function startSession()
	{
		    if (session_status() == PHP_SESSION_NONE || session_id() == '')
		    {
		    	session_start();
		    	$this->currentSessionID = session_id(); 
		    } 
		    return $this->currentSessionID ;
	}
	
	public function getCurrentSessionID()
	{
		return $this->currentSessionID;
	}
	
	public function checkUserSession()
	{
		 if(isset($_SESSION['login']))
		 	return true;
	}
	
	public function checkUserCanUploadTest()
	{
		if($this->checkUserSession()) 
		{
			if($this->checkIsAdmin() === 1) return true;
			else return false;
		}
	}
	
	public function checkUserCanDeleteTest()
	{
		if($this->checkUserSession()) 
		{
			if($this->checkIsAdmin() === 1) return true;
			else return false;
		}
	}
	
	public function checkUserCanReadListOfTest()
	{
		if($this->checkUserSession()) return true;
	}
	
	public function checkIsAdmin()
	{
		if($this->checkUserSession()) return $_SESSION['is_admin'];
		else return 0;
	}
	
	public function checkUser($userLogin, $userPasswd)
	{
		 $this->getAllUsers();
		 if(!isset($this->usersData) || empty($this->usersData))
		 		exit('Невозможно проверить полномочия пользователя');
		 if(array_key_exists ($userLogin,$this->usersData) && 
		 $this->usersData["$userLogin"]['password'] === $userPasswd)
		 {
		 		$_SESSION['is_admin'] =  $this->usersData["$userLogin"]['is_admin']; 
        		$_SESSION['login'] = $userLogin;
         		$_SESSION['password'] = $userPasswd;	
			 	return true;
		 }
		 else 
		 	return false;	
	}
	
	protected function getAllUsers()
	{
		$rootDir = str_replace('classes/Authorization','',__DIR__);     
		$filename = $rootDir.'/persons_data/persons.json';		
		$jsonData = file_get_contents($filename);
		$this->usersData = json_decode($jsonData,true);
		if(json_last_error() !== JSON_ERROR_NONE) $this->usersData = [];
	}
	
    public function logOut()
	{
			session_destroy();   
			session_unset();
			unset($this->currentSessionID);
	}
	
	public function showForm()
	{
		return '<form action="" method="post">
		<div class="form-group">
		<label for="login">Login</label>
			<input type="text" class="form-control" name="login" id="login" placeholder="Введите Ваш login" required>
		</div>
		<div class="form-group">
		<label for="password">Пароль</label>
			<input type="password" class="form-control" name="password" id="password" placeholder="Введите пароль" required>
		</div>
		<button type="submit" class="btn btn-primary">Авторизоваться</button>
		</form>';	
	}	
	
	public function showAuthorizationError()
	{
		return '<div class="panel panel-info"> <div class="panel-heading"> <h3 class="panel-title">Ошибка авторизации</h3> </div> <div class="panel-body"> Неправильный логин или пароль </div> </div>';
	}
	
	public function showForbidden(){
		   	http_response_code(403);
            echo '<h1>Нет прав на просмотр страницы</h1>';
	}
}
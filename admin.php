<?php
    include __DIR__.'/page_components/header.php';
    if(!empty($_POST)&&array_key_exists('testName', $_POST))
    {
        $testName = $_POST['testName'];
        if(!empty($_FILES)&&array_key_exists('testFile', $_FILES))
        {
            if (strpos($_FILES['testFile']['type'], 'json') !== false) {
                    $filename = $dataInstance->saveNewTest($testName);
            }
         }
    }
    $header = $pageComponents->getHeader($canUploadTest, $canReadListOfTest);
    echo  $header;
    if(!$authorization->checkUserSession() || $authorization->checkIsAdmin() != 1)
    {
            $authorization->showForbidden();
            include __DIR__.'/page_components/footer.php';
            die;
    }
?>
    <h1>Добавить новый тест</h1>
    <form enctype="multipart/form-data" action="admin.php" method="post">                    
        <label style="margin: 20px 0 10px;" for="testName">Укажите название теста</label>  
        <input style="margin: 0 0 20px;" id="testName" class="form-control" name="testName" type="text" />
        <label style="margin: 20px 0 10px;" for="testFile">Загрузить файл с тестом (допустимый формат JSON)</label>  
        <input style="margin: 0 0 20px;" id="testFile" class="form-control" name="testFile" type="file" />
        <button type="submit" class="btn btn-primary">Отправить</button>
    </form>
<?php 	 
   include __DIR__.'/page_components/footer.php';
<?php
   include 'page_components/header.php';
   if(!empty($_GET) && array_key_exists('action', $_GET) && array_key_exists('test', $_GET))
    {
        if(($authorization->checkIsAdmin() == 1) && $_GET['action'] === 'delete'){
             $filename = $dataInstance->getTestNamefromGet( $_GET['test'] );
             unlink ($filename);
        }
    }
    $header = $pageComponents->getHeader($canUploadTest, $canReadListOfTest);
    echo  $header;
    if(!$authorization->checkUserSession())
    {
 			$authorization->showForbidden();
 			include 'page_components/footer.php';
 			die;
    }
?>
     <h1>Список тестов</h1>
     <table class="table" style="width: 800px; margin:auto">
	     <thead>
	     <tr>
	      <th scope="col">Название теста</th>
	      <th scope="col">Тест</th>
	       <?php if($authorization->checkIsAdmin() == 1){
	                echo '<th scope="col">Опции</th>';
	       } ?>
	     </tr>
	    </thead>
	    <?php
	        $tests = $dataInstance->getListOfTests();
	        foreach($tests as $test){
	            $name_len = strlen ( $test );
	            $start = strpos( $test, 'json/')+ strlen(  'json/' );    
	            $name = explode('_', substr($test , $start));
	            echo '<tr>';
	            echo '<td scope="col">'.$name[0].'</td>';
	            echo '<td scope="col"><a href="test.php?test='.$name[2].'">Перейти к тесту</a></td>';
	            if($authorization->checkIsAdmin() == 1){
	                echo '<td scope="col"><a href="?test='.$name[2].'&action=delete">Удалить тест</a></td>';
	            }
	            echo '</tr>';
	        }
	    ?>
    </table>
<?php
    include 'page_components/footer.php';